<?php
namespace Swissclinic\GermanLawFix\Plugin;


use Magento\Framework\Pricing\Render;

class AfterPrice extends \Magenerds\GermanLaw\Model\Plugin\AfterPrice
{
    /**
     * Renders and caches the after price html
     *
     * @return null|string
     */
    protected function _getAfterPriceHtml()
    {
        if (null === $this->_afterPriceHtml) {
            $afterPriceBlock = $this->_layout->createBlock('Swissclinic\GermanLawFix\Block\AfterPrice', 'after_price');
            $afterPriceBlock->setTemplate('Magenerds_GermanLaw::price/after.phtml');
            $this->_afterPriceHtml = $afterPriceBlock->toHtml();
        }

        return $this->_afterPriceHtml;
    }
}