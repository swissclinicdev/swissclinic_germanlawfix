<?php
namespace Swissclinic\GermanLawFix\Block;


class AfterPrice extends \Magenerds\GermanLaw\Block\AfterPrice
{
    public function getTaxText()
    {
        $product = $this->_registry->registry('product');
        if(!$product) {
            return '';
        } else {
            return parent::getTaxText();
        }
    }
}